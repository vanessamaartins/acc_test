To run Pipeline:

$ cd acc_test
$ docker-compose up

Go to http://jenkinsHost:port/scriptApproval/
And approve the signatures below after running the pipeline at least once:
- method hudson.model.AbstractCIBase getNodes
- staticMethod jenkins.model.Jenkins getInstance

Use the jenkinsfile available on the project
